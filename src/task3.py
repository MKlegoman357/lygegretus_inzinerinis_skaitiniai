import math
import random
import time

import matplotlib.pyplot as plt
import numpy as np
import multiprocessing as mp


def target(points, s):
    lengths = np.array(
        [
            math.sqrt(sum(np.square(points[i] - points[j])))
            for i in range(len(points) - 1) for j in range(i + 1, len(points))
        ]
    ).flatten()
    average_length = np.average(lengths)
    average_diff = sum(np.abs(lengths - average_length))

    s_diff = abs(sum(lengths) - s)

    return average_diff + s_diff


def point_gvazi_gradient(i, point, points, current_target, delta, target, *args, **kwargs):
    point_gradient = []

    for j, coord in enumerate(point):
        if i == 0:  # the first point [0, 0] is fixed in place
            point_gradient.append(0)
        else:
            modified_points = points.copy()
            modified_points[i][j] += delta

            point_gradient.append((target(modified_points, *args, **kwargs) - current_target) / delta)

    point_gradient = np.array(point_gradient)

    if point_gradient.any():
        point_gradient /= np.linalg.norm(point_gradient)

    return point_gradient


def qvazi_gradient(points, delta, target, *args, **kwargs):
    current_target = target(points, *args, **kwargs)
    gradient = []

    for i, point in enumerate(points):
        gradient.append(point_gvazi_gradient(i, point, points, current_target, delta, target, *args, **kwargs))

    return np.array(gradient)


def qvazi_gradient_part(index, num_per_thread, points, current_target, delta, target, *args, **kwargs):
    start_index = index * num_per_thread
    end_index = min((index + 1) * num_per_thread, len(points))
    gradient = []

    for i in range(start_index, end_index):
        gradient.append(point_gvazi_gradient(i, points[i], points, current_target, delta, target, *args, **kwargs))

    return gradient


def parallel_qvazi_gradient(pool, thread_count, points, delta, target, *args):
    current_target = target(points, *args)
    gradient = []
    results = []

    num_per_thread = math.ceil(len(points) / thread_count)

    for index in range(thread_count):
        results.append(pool.apply_async(
            qvazi_gradient_part, [index, num_per_thread, points, current_target, delta, target, *args]
        ))

    for result in results:
        gradient += result.get()

    return np.array(gradient)


def broiden_gradient(points, prev_points, prev_gradient, target, prev_target):
    s = (points - prev_points).flatten("F")[None].T
    y = target - prev_target

    prev_gradient = prev_gradient.flatten("F")

    gradient = prev_gradient + np.matmul(y - np.matmul(prev_gradient, s), s.T) / np.matmul(s.T, s)

    gradient = np.reshape(gradient, points.shape, "F")

    for point in gradient:
        if point.any():
            point /= np.linalg.norm(point)

    return gradient


def main_animated():
    num_points = 4
    x_min, x_max, y_min, y_max = -10, 10, -10, 10
    s_target = 10
    step = 1
    qvazi_gradient_delta = 0.001
    iterations = 100
    accuracy = 1e-3

    random.seed(100)

    points = np.array(
        [[0.0, 0.0]] +
        [[random.uniform(x_min, x_max), random.uniform(y_min, y_max)] for i in range(num_points - 1)]
    )

    gradient_total_time = 0

    gradient_time_start = time.perf_counter_ns()
    gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
    gradient_total_time += time.perf_counter_ns() - gradient_time_start

    current_target = target(points, s_target)

    print(points, "points")
    print(gradient, "initial gradient")

    for i in range(iterations):
        plt.clf()
        plt.xlim(x_min, x_max)
        plt.ylim(y_min, y_max)
        plt.scatter(points[:, 0], points[:, 1])
        plt.title(f"Target: {current_target} | Iteration: {i + 1}")

        # plt.pause(0.001)
        # plt.waitforbuttonpress()

        prev_points = points
        points = prev_points - step * gradient
        prev_target = current_target
        current_target = target(points, s_target)

        if current_target <= accuracy:
            break

        if prev_target < current_target:
            gradient_time_start = time.perf_counter_ns()
            gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
            gradient_total_time += time.perf_counter_ns() - gradient_time_start
            step *= 0.95
        else:
            gradient = broiden_gradient(points, prev_points, gradient, current_target, prev_target)

    print(f"Gradient average took {gradient_total_time / (iterations + 1)} ns")

    plt.show()


def main_animated_parallel():
    num_points = 4
    x_min, x_max, y_min, y_max = -10, 10, -10, 10
    s_target = 10
    step = 1
    qvazi_gradient_delta = 0.001
    iterations = 100
    accuracy = 1e-3
    thread_count = 4

    random.seed(100)

    gradient_total_time = 0

    with mp.Pool(thread_count) as pool:
        points = np.array(
            [[0.0, 0.0]] +
            [[random.uniform(x_min, x_max), random.uniform(y_min, y_max)] for i in range(num_points - 1)]
        )

        gradient_time_start = time.perf_counter_ns()
        # gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
        gradient = parallel_qvazi_gradient(pool, thread_count, points, qvazi_gradient_delta, target, s_target)
        gradient_total_time += time.perf_counter_ns() - gradient_time_start
        current_target = target(points, s_target)

        print(points, "points")
        print(gradient, "initial gradient")

        for i in range(iterations):
            plt.clf()
            plt.xlim(x_min, x_max)
            plt.ylim(y_min, y_max)
            plt.scatter(points[:, 0], points[:, 1])
            plt.title(f"Target: {current_target} | Iteration: {i + 1}")

            # plt.pause(0.001)
            # plt.waitforbuttonpress()

            prev_points = points
            points = prev_points - step * gradient
            prev_target = current_target
            current_target = target(points, s_target)

            if current_target <= accuracy:
                break

            if prev_target < current_target:
                gradient_time_start = time.perf_counter_ns()
                # gradient = qvazi_gradient(points, qvazi_gradient_delta, target, s_target)
                gradient = parallel_qvazi_gradient(pool, thread_count, points, qvazi_gradient_delta, target, s_target)
                gradient_total_time += time.perf_counter_ns() - gradient_time_start
                step *= 0.95
            else:
                gradient = broiden_gradient(points, prev_points, gradient, current_target, prev_target)

    print(f"Gradient average took {gradient_total_time / (iterations + 1)} ns")

    plt.show()


if __name__ == "__main__":
    main_animated()
    main_animated_parallel()
