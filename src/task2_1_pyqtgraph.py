import math

import numpy as np
import pyqtgraph as pg
import pyqtgraph.opengl as gl

pg.mkQApp()

view = gl.GLViewWidget()
view.setBackgroundColor(64, 64, 64)
view.show()
view.setCameraPosition(distance=50)

grid = gl.GLGridItem()
view.addItem(grid)


def z1(x, y):
    """
    (x^2 + y^2) / 5 - 2cos(x/2) - 6cos(y) - 8
    """
    return (x ** 2 + y ** 2) / 5 - 2 * math.cos(x / 2) - 6 * math.cos(y) - 8


def z2(x, y):
    """
    (x/2)^5 + (y/2)^4 - 4
    """
    return (x / 2) ** 5 + (y / 2) ** 4 - 4


def createSurfacePlot(x_from, x_to, y_from, y_to, step, z_func, color=(.5, .5, .5, .75)):
    x = np.arange(x_from, x_to, step)
    y = np.arange(y_from, y_to, step)
    z = np.array([[z_func(x_coord, y_coord) for x_coord in x] for y_coord in y])

    surface = gl.GLSurfacePlotItem(x, y, z, shader="shaded", color=pg.glColor(color), glOptions="translucent")

    return surface


def createWireframePlot(x_from, x_to, y_from, y_to, step, z_func, resolution=10, color=(.5, .5, .5, .75)):
    x = np.arange(x_from, x_to, step / resolution)
    y = np.arange(y_from, y_to, step / resolution)
    z = np.array([[z_func(x_coord, y_coord) for x_coord in x] for y_coord in y])

    lines = []

    lines.extend([
        gl.GLLinePlotItem(pos=np.array([
            [x_coord, y_coord, z[x_index][y_index]] for y_index, y_coord in enumerate(y)
        ]), color=pg.glColor(color)) for x_index, x_coord in enumerate(x) if x_index % resolution == 0
    ])

    lines.extend([
        gl.GLLinePlotItem(pos=np.array([
            [x_coord, y_coord, z[x_index][y_index]] for x_index, x_coord in enumerate(x)
        ]), color=pg.glColor(color)) for y_index, y_coord in enumerate(y) if y_index % resolution == 0
    ])

    return lines


plot_size = 10
plot_step = 1

# surface1 = createSurfacePlot(-plot_size, plot_size, -plot_size, plot_size, plot_step, z1, color="5555cccc")
# view.addItem(surface1)

# surface2 = createSurfacePlot(-plot_size, plot_size, -plot_size, plot_size, plot_step, z2, color="55cc55cc")
# view.addItem(surface2)

wireframe1 = createWireframePlot(-plot_size, plot_size, -plot_size, plot_size, plot_step, z1, color="5555cccc")
for line in wireframe1:
    view.addItem(line)

wireframe2 = createWireframePlot(-plot_size, plot_size, -plot_size, plot_size, plot_step, z2, color="55cc55cc")
for line in wireframe2:
    view.addItem(line)
