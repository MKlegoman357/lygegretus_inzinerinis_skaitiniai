import numpy as np
import plotly.graph_objects as go
import plotly.basedatatypes as bdt


def plot_surface(x_from, x_to, y_from, y_to, z_func, step, **kwargs) -> bdt.BaseTraceType:
    x = np.arange(x_from, x_to + step, step, dtype=float)
    y = np.arange(y_from, y_to + step, step, dtype=float)
    xm, ym = np.meshgrid(x, y, sparse=True)
    z = np.array([[z_func(y_value, x_value) for x_value in y] for y_value in x])

    return go.Surface(x=x, y=y, z=z, **kwargs)


def plot_surface_with_contour(x_from, x_to, y_from, y_to, z_func, step, **kwargs) -> (bdt.BaseTraceType, bdt.BaseTraceType):
    x = np.arange(x_from, x_to + step, step, dtype=float)
    y = np.arange(y_from, y_to + step, step, dtype=float)
    xm, ym = np.meshgrid(x, y, sparse=True)
    z = np.array([[z_func(y_value, x_value) for x_value in y] for y_value in x])

    return go.Surface(x=x, y=y, z=z, **kwargs), go.Contour(
        x=x, y=y, z=z,
        showscale=False, contours_coloring="lines",
        contours=dict(start=0, end=0, size=0),
        colorscale="Blues"
    )
