import numpy as np
import plotly.graph_objects as go


def z1(x, y):
    """
    (x^2 + y^2) / 5 - 2cos(x/2) - 6cos(y) - 8
    """
    return (x ** 2 + y ** 2) / 5 - 2 * np.cos(x / 2) - 6 * np.cos(y) - 8


def z2(x, y):
    """
    (x/2)^5 + (y/2)^4 - 4
    """
    return (x / 2) ** 5 + (y / 2) ** 4 - 4


def plot_surface(x_from, x_to, y_from, y_to, z_func, step):
    x = np.arange(x_from, x_to, step, dtype=float)
    y = np.arange(y_from, y_to, step, dtype=float)
    x, y = np.meshgrid(x, y, sparse=True)
    z = z_func(x, y)

    return go.Surface(x=x, y=y, z=z)


plot_size = 10
plot_step = 1

surface1 = plot_surface(-plot_size, plot_size, -plot_size, plot_size, z1, plot_step)

fig = go.Figure(data=[surface1])

fig.update_layout(title="foo", autosize=False, width=500, height=500)

fig.show()
