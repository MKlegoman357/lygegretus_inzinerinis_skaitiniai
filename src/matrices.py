def gauss_jordan_elimination(matrix, print_intermediate_results=False):
    """
    adapted from gauss-jordan algorithm described in:
    https://newonlinecourses.science.psu.edu/statprogram/reviews/matrix-algebra/gauss-jordan-elimination
    """

    row_count = matrix.shape[0]

    # move zero rows down
    zero_rows = (~matrix.any(axis=1)).nonzero()[0]

    j = row_count - 1
    for i in reversed(zero_rows):
        matrix[[j, i]] = matrix[[i, j]]
        j -= 1

    for i in range(row_count):
        # move the row with the largest leftmost nonzero entry to the top
        max_leftmost_index = i
        for j in range(i + 1, row_count):
            if abs(matrix[j][i]) > abs(matrix[max_leftmost_index][i]):
                max_leftmost_index = j
        matrix[[i, max_leftmost_index]] = matrix[[max_leftmost_index, i]]

        row = matrix[i]

        # multiply the row by the leftmost nonzero entry to make the entry equal 1.0
        row /= row[i]

        # subtract the multiplied top row to the other rows to zero-out the i-th column
        for j in range(row_count):
            if j != i:
                matrix[j] -= row * matrix[j][i]

        if print_intermediate_results:
            print(f"Matrix after processing column {i}:\n", matrix)

    return matrix
