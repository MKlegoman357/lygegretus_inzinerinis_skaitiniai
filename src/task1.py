import numpy as np

import matrices

# 4x1 + x2 + x3 + 7x4 = 148
# x1 + 2x3 - 2x4 = -37
# 2x1 + 2x2 - 7x3 + x4 = 21
# 4x1 + 14x2 + 7x3 = 53

print("AX=B")

A = np.array(
    [[4, 1, 1, 7],
     [1, 0, 2, -2],
     [2, 2, -7, 1],
     [4, 14, 7, 0]],
    dtype=float
)

B = np.array([[148, -37, 21, 53]], dtype=float).T

print("A:\n", A)
print("B:\n", B)

augmentedAB = np.concatenate((A, B), axis=1).astype(float)

print("A | B:\n", augmentedAB)

augmentedAB = matrices.gauss_jordan_elimination(augmentedAB, print_intermediate_results=True)

print("After Gauss-Jordan elimination:")
print("A | B:\n", augmentedAB)

X = augmentedAB[:, [-1]]

print("X:\n", X)

print()
print("Result checking:")

resultingB = np.matmul(A, X)

print("A*X:\n", resultingB[:, 0])
print("Expected result (B):\n", B[:, 0])
