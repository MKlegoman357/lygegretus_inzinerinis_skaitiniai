
% Greiciausio nusileidimo metodas

function pagrindine
  clc,close all
  
  eps=1e-6
  step0=1; step=step0;
  itmax=300
  x=[4.05;5.05;6.05;-1.05];
  
  for iii=1:itmax  % ciklas per minimizavimo kryptis
    
    grad=gradient(x);  fff=target(x);
    while 1==1  % minimizavimas pagal parinkta krypti
      deltax=grad/norm(grad)*step; x=x-deltax'; fff1=target(x);
      
      if fff1 > fff,
        x=x+deltax'; step=step/1.05; break;
        else,
        fff=fff1;
      end  %*****var1****
      %         if fff1 > fff, x=x+deltax'; step=step/10; break; else, fff=fff1;end  %*****var2****
    end
    %     step=step0;  %***var2******
    
    tikslumas=norm(fff);
    fprintf(1,'\n kryptis %d  tikslumas %g zingsnis %g',iii,tikslumas,step);
    if tikslumas < eps,  fprintf(1,'\n sprendinys x ='); fprintf(1,'  %g\n',x); break
    elseif iii == itmax, fprintf(1,'\n ****tikslumas nepasiektas. Paskutinis artinys x =');fprintf(1,'  %g\n',x); break
    end
    
  end
  
  return
end

%   Lygciu sistemos funkcija 
% 2x2+x3+2x4-14=0
% 3x4^3+3x2x4+18=0
% -2x1^2+5x2^3-3x3^2-485=0
% 5x1-6x2+3x3-3x4-11=0
function F=f(X) 
  F(1)=2*X(2)+X(3)+2*X(4)-14;
  F(2)=3*X(4)^3+3*X(2)*X(4)+18;
  F(3)=-2*X(1)^2+5*X(2)^3-3*X(3)^2-485;
  F(4)=5*X(1)-6*X(2)+3*X(3)-3*X(4)-11;
  F=F(:);
  return
end 

%     Tikslo funkcija
function rez=target(x)
  rez=f(x)'*f(x)/2;
  return
end

%  Jakobio matrica
function DF=df(X)
  DF(1,1)=0; DF(1,2)=2; DF(1,3)=1; DF(1,4)=2;
  DF(2,1)=0; DF(2,2)=3*X(4); DF(2,3)=0; DF(2,4)=9*X(4)^2+3*X(2);
  DF(3,1)=-4*X(1); DF(3,2)=15*X(2)^2; DF(3,3)=-6*X(3); DF(3,4)=0;
  DF(4,1)=5; DF(4,2)=-6; DF(4,3)=3; DF(4,4)=-3;
  return
end

% Gradientas
function rez=gradient(x)
  rez=f(x)'*df(x);
  return
end
